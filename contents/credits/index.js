$(document).ready(function () {
  $.get('link.json', (data) => {
    let _links = data;
    $('.link').each(function () {
      try {
        let link = _links[$(this).attr('data-link')];
        if ($(this).is('img')) {
          $(this).prop("src", link);
        } else if ($(this).is('object')) {
          $(this).prop("data", link);
        }
      } catch (e) {
        console.log(e);
      }
    });
  }, 'json');
});